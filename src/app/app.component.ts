import { Component, AfterViewInit, ViewChild, ElementRef, EventEmitter } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Row } from './app.component.model';
import { Dose } from './calculator.service';
declare var moment: any;
declare var $: any;
declare var M: any;

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements AfterViewInit {
  public readonly caffeineEntryForm: FormGroup;
  public readonly title: string = 'Caffeind';
  public outerRows: Row[] = [];
  public readonly updater: EventEmitter<Dose> = new EventEmitter();
  @ViewChild('submissionForm') private readonly submitForm: ElementRef<HTMLFormElement>;
  @ViewChild('time') private readonly timeInput: ElementRef<HTMLFormElement>;

  constructor(private fb: FormBuilder) {
    this.caffeineEntryForm = fb.group({
      dosage: [null, Validators.required],
      time: [null, Validators.required]
    });
  }

  public timeToMoment(time: string): number {
    return moment(time, 'HH:mm').valueOf();
  }

  private reset() {
    this.submitForm.nativeElement.reset();
  }

  private appendRow(entry: Dose): void {
    const time = this.timeToMoment(`${entry.time}`);
    this.updater.emit({
      dosage: entry.dosage,
      time: time
    });
    this.caffeineEntryForm.reset({ emitEvent: true });
  }

  public onCloseStart(): void {
    const currentValue = this.timeInput.nativeElement.value;
    if (!currentValue) {
      return;
    }
    this.caffeineEntryForm.get('time').setValue(currentValue);

    // manually trigger blur to update the form's state
    const blur = new Event('blur');
    this.timeInput.nativeElement.dispatchEvent(blur);
  }

  public ngAfterViewInit() {
    const form = this.caffeineEntryForm;
    const onCloseStart: Function = this.onCloseStart.bind(this);
    $(document).ready(() => {
      $('.modal').modal({
        dismissible: true,
        endingTop: '10%',
        inDuration: 300,
        opacity: 0.5,
        outDuration: 200,
        ready: this.reset,
        startingTop: '4%',
        onCloseStart: () => {
          if (form.pristine || form.invalid) {
            return;
          }
          const entry = form.value;
          this.appendRow(entry);
        }
      });

      $('.timepicker').timepicker({ onCloseStart });
    });
  }
}
