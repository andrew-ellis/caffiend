export interface Row {
  readonly value: number;
  readonly time: string;
}
