import { Injectable } from '@angular/core';
import { Row } from './app.component.model';

export interface Dose {
  readonly dosage: number;
  readonly time: number;
}

const sixHours = 6 * 36e5;

function add(prev: number, curr: number): number {
  return prev + curr;
}

function doseReduce(prev: { mg: number }, nextDose: Dose): { mg: number } {
  return { mg: prev.mg + nextDose.dosage };
}

interface UpdateObj {
  dailyTotal: number;
  currentCaffeine: number;
  sixHourTotal: number;
  twelveHourTotal: number;
}
@Injectable({
  providedIn: 'root'
})
export class CalculatorService {
  private currentPotency(dose: Dose, future?: number): number {
    const initialDose = dose.dosage;
    const endTime = future ? future : Date.now();
    const diff = Math.abs(endTime - dose.time) / 60 / 60 / 1000;

    return Math.round(initialDose * Math.pow(1 / 2, diff / 6));

    // console.log(`${diff} hours have passed, resulting in ${currentPotency}mg remaining`);
  }

  public updateCurrentCaffeine(entries: Dose[]): UpdateObj {
    const initialDose = { mg: 0 };
    const dailyTotal = entries.reduce(doseReduce, initialDose).mg;

    const currentCaffeine = entries.map(dose => this.currentPotency(dose)).reduce(add, 0);
    const sixHourTotal = entries.map(dose => this.currentPotency(dose, Date.now() + sixHours)).reduce(add, 0);
    const twelveHourTotal = entries.map(dose => this.currentPotency(dose, Date.now() + sixHours * 2)).reduce(add, 0);

    return {
      dailyTotal,
      currentCaffeine,
      sixHourTotal,
      twelveHourTotal
    };
  }
}
