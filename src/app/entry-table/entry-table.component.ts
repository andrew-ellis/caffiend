import { Component, Input, EventEmitter, OnInit, ChangeDetectionStrategy, ChangeDetectorRef } from '@angular/core';
import { CalculatorService, Dose } from '../calculator.service';

@Component({
  selector: 'app-entry-table',
  templateUrl: './entry-table.component.html',
  styleUrls: ['./entry-table.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class EntryTableComponent implements OnInit {
  @Input() public readonly onUpdate: EventEmitter<Dose>;
  public rows: Dose[] = [];
  public dailyTotal: number;
  public currentCaffeine: number;
  public sixHourTotal: number;
  public twelveHourTotal: number;

  constructor(private calc: CalculatorService, private cd: ChangeDetectorRef) {
    this.dailyTotal = 0;
    this.currentCaffeine = 0;
    this.sixHourTotal = 0;
    this.twelveHourTotal = 0;
  }

  private updateRows(data: Dose): void {
    this.rows.push(data);

    const { currentCaffeine, twelveHourTotal, sixHourTotal, dailyTotal } = this.calc.updateCurrentCaffeine(this.rows);
    this.currentCaffeine = currentCaffeine;
    this.twelveHourTotal = twelveHourTotal;
    this.sixHourTotal = sixHourTotal;
    this.dailyTotal = dailyTotal;

    this.cd.markForCheck();
    this.cd.detectChanges();
  }

  public ngOnInit() {
    this.onUpdate.subscribe(this.updateRows.bind(this));
  }
}
